import { goto } from "$app/navigation"
import { Cntral } from "./cntral"

CHECK_AUTH_LOCK = true

function checkAuth(forced) {
    var token = getToken()

    if (!token) { redirectOnAuth('none') }

    Cntral({
        action: 'check_auth',
        payload: token
    }).then(function (response) {
        if (response.ok) {
            return response.text()
        }

        if (response.status == 502) {
            setTimeout(checkAuth, 5000)
            return Promise.reject("Сервер недоступен.")
        }

        return Promise.reject("Ошибка обработки запроса.")
    }).then(function (mode) {
        if (mode == "expired") { invalidateToken() }

        redirectOnAuth(mode, forced)
    }).catch(function (message) {
        console.log(message)
        CHECK_AUTH_LOCK = false
    })
}

function redirectOnAuth(mode, forced) {
    var transitions = {
        none: "login",
        expired: "login",
        email: "select",
        user: "/",
    }

    var newLocation = transitions[mode]
    if (newLocation && (forced || newLocation != CURRENT_LOCATION)) {
        try { goto(newLocation) }
        catch (e) { goto(newLocation) }
    }

    CHECK_AUTH_LOCK = false
}



export default { checkAuth, redirectOnAuth }

