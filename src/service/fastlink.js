import { getToken, setToken } from "./tokens"
import { Cntral } from "./cntral"
import { goto } from "$app/navigation"


export function fastlink() {
    var message = document.getElementById("message")

    if (!checkBrowser()) {
        browserTooOld(message)
        return
    }

    var link = decodeURIComponent(window.location.search.substring(1))

    var components = link.trim().split(/\?/)

    var upgrade = components[0] == 'u'
    if (upgrade) { components.splice(0, 1) }

    var auth = getToken()

    link = components[0]
    const action = (upgrade && auth) 
      ? { action: "upgrade_fastlink", payload: { 
          fastlink:link, token: auth
        }}
      : { action: "use_fastlink", payload: link }
    var navigation = components[1] ? ("?" + components[1]) : ""

    Cntral(action).then(function (response) {
        return Promise.all([
            response.text(),
            Promise.resolve(response)
        ])
    }).then(function (tr) {
        var text = tr[0]
        var response = tr[1]

        if (response.ok) {
            var bad = false
            try { setToken(text) }
            catch (e) { alert("Включите в браузере локальное хранилище и обновите страницу"); bad = true }

            if (!bad) {
                var newLocation = navigation

                try { goto(newLocation) }
                catch (e) { goto(newLocation) }
            }

            return
        }

        if (text == "wrong_fastlink_token") {
            message.innerHTML = "Некорректная быстрая ссылка"
        }
        else if (text == "outdated_fastlink") {
            message.innerHTML = "Быстрая ссылка устарела"
        }
        else {
            message.innerHTML = "Попытка входа не удалась [" + text + "], попробуйте ещё раз, обновив страницу"
        }
    }).catch(function () {
        message.innerHTML = "Не удаётся установить соединение с сервером. Обновите страницу через несколько минут."
    })
}


function checkBrowser() {
    var checked = false

    try {
        var f = eval("(a = 0) => a")
        if (f() == 0) { checked = true }
    } catch (err) { }

    return checked
}


function browserTooOld(message) {
    var text = "<p>Ваш браузер устарел и не поддерживается. Поддерживаются следующие браузеры:</p>"
    text += "<ul>"
    text += "<li>Mozilla Firefox версии <em>не ниже</em> 52</li>"
    text += "<li>Google Chrome версии <em>не ниже</em> 57</li>"
    text += "</ul>"
    text += "<p>Если Ваш браузер удовлетворяет вышеуказанным ограничениям, отключите <em>все</em> дополнения, а затем обновите страницу: "
    text += "блокировщики рекламы могут вмешиваться в работу функции проверки совместимости.</p>"

    message.innerHTML = text
}