export function invalidateToken() {
    if (!sessionStorage || !localStorage) { return }

    const sessionToken = sessionStorage.getItem('auth')
    const localToken = sessionStorage.getItem('auth')

    if (!sessionToken || sessionToken === localToken) {
      localStorage.removeItem('auth')
      localStorage.removeItem('uid')
    }

    sessionStorage.removeItem('auth')
    sessionStorage.removeItem('uid')
}

export function getToken() {
    if (!sessionStorage || !localStorage) { return }

    const sessionToken = sessionStorage.getItem('auth')
    if (sessionToken) { return sessionToken }

    if (!localStorage) { return undefined }

    return localStorage.getItem('auth')
}

export function getUid() {
    if (!sessionStorage || !localStorage) { return }

    const sessionUid = sessionStorage.getItem('uid')
    if (sessionUid) { return sessionUid }

    return localStorage.getItem('uid')
}

export function setToken(token) {
    if (!sessionStorage || !localStorage) { return }

    sessionStorage.setItem('auth', token)
    localStorage.setItem('auth', token)
}

export function setUid(uid) {
    if (!sessionStorage || !localStorage) { return }

    sessionStorage.setItem('uid', `${uid}`)
    localStorage.setItem('uid', `${uid}`)
}